#include "intstr.h"

int64_t str_int64(std::string str)
{
    int64_t value = 0;
    int pos = str.length() - 1;
    int64_t factor = 1;
    while (pos >= 0)
    {
        switch (str.at(pos))
        {
            case '1': value += 1*factor; break; case '2': value += 2*factor; break;
            case '3': value += 3*factor; break; case '4': value += 4*factor; break;
            case '5': value += 5*factor; break; case '6': value += 6*factor; break;
            case '7': value += 7*factor; break; case '8': value += 8*factor; break;
            case '9': value += 9*factor; break; case '0': value += 0*factor; break;
            default: return 0; // non-numeric character found
        };
        pos--;
        factor *= 10;
    }
    return value;
}

int64_t str_int64date(std::string str)
{
    int64_t date;

    if (str.length() == 8) // YYYYMMDD hopefully
    {
        // definitely YYYYMMDD
        if (str.find_first_not_of("0123456789") == std::string::npos)
        {
            date =
                str_int64(str.substr(0,4)) * 10000000000 +
                str_int64(str.substr(4,2)) * 100000000 +
                str_int64(str.substr(6,2)) * 1000000;
        }
        else date = 0;
    }
    else if (str.length() == 10) // YYYY-MM-DD or similar hopefully
    {
        // definitely YYYY-MM-DD or similar
        if (str.at(4) == str.at(7))
        {
            date =
                str_int64(str.substr(0,4)) * 10000000000 +
                str_int64(str.substr(5,2)) * 100000000 +
                str_int64(str.substr(8,2)) * 1000000;
        }
        else date = 0;
    }
    else if (str.length() == 14) // YYYYMMDDHHMMSS hopefully
    {
        // definitely YYYYMMDDHHMMSS
        if (str.find_first_not_of("0123456789") == std::string::npos)
        {
            date =
                str_int64(str.substr(0,4))  * 10000000000 +
                str_int64(str.substr(4,2))  * 100000000 +
                str_int64(str.substr(6,2))  * 1000000 +
                str_int64(str.substr(8,2))  * 10000 +
                str_int64(str.substr(10,2)) * 100 +
                str_int64(str.substr(12,2)) * 1;
        }
        else date = 0;
    }
    else if (str.length() == 19) // YYYY-MM-DD HH:MM:SS or similar hopefully
    {
        // definitely YYYY-MM-DD HH:MM:SS or similar
        if (str.at(4)==str.at(7) && str.at(13)==str.at(16))
        {
            date =
                str_int64(str.substr(0,4))  * 10000000000 +
                str_int64(str.substr(5,2))  * 100000000 +
                str_int64(str.substr(8,2))  * 1000000 +
                str_int64(str.substr(11,2)) * 10000 +
                str_int64(str.substr(14,2)) * 100 +
                str_int64(str.substr(17,2)) * 1;
        }
        else date = 0;
    }
    else date = 0;
    return date;
}

std::string int64_str(int64_t num, unsigned int minNumDigits)
{
    std::string str = "";
    if (num < 0)
    {
        str += "-";
        num *= -1;
    }

    int digit;
    bool haveDisplayed = false;
    for (int64_t fac = 1000000000000000000; fac >= 1; fac /= 10)
    {
        digit = num / fac;
        if ((digit != 0 && !haveDisplayed) || (fac == 1 && !haveDisplayed)) haveDisplayed = true;
        if (haveDisplayed)
        {
            switch (digit)
            {
                case 0: str += "0"; break; case 1: str += "1"; break;
                case 2: str += "2"; break; case 3: str += "3"; break;
                case 4: str += "4"; break; case 5: str += "5"; break;
                case 6: str += "6"; break; case 7: str += "7"; break;
                case 8: str += "8"; break; case 9: str += "9"; break;
            }
        }
        num -= fac * digit;
    }
    if (str.length() < minNumDigits)
    {
        str = std::string(minNumDigits - str.length(), '0') + str;
    }
    return str;
}
