#ifndef EDITINPUTWINDOW_H
#define EDITINPUTWINDOW_H

#include <QWidget>
#include <basis.h>
#include <ratefetcher.h>

namespace Ui {
class EditInputWindow;
}

class EditInputWindow : public QWidget
{
    Q_OBJECT
    
public:
    explicit EditInputWindow(
            Basis::Basis *curBasis,
            QWidget *mainWin,
            QWidget *parent = 0);
    ~EditInputWindow();

signals:
    void update_lineEdit_rate(QString);
    void done();
private slots:
    void on_lineEdit_amount_textChanged(const QString &arg1);
    void on_lineEdit_cost_textChanged(const QString &arg1);
    void on_lineEdit_fee_textChanged(const QString &arg1);
    void on_pushButton_accept_clicked();   
    void on_checkBox_fetchRate_toggled(bool checked);

    void on_pushButton_cancel_clicked();

private:
    Ui::EditInputWindow *ui;

    Basis::Input* pendingInput;
    Basis::Basis* basis;
    QWidget *mainWindow;
    RateFetcher rateFetcher;
};

#endif // EDITINPUTWINDOW_H
