#include "editinputwindow.h"
#include "ui_editinputwindow.h"

EditInputWindow::EditInputWindow(
        Basis::Basis *curBasis,
        QWidget *mainWin,
        QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EditInputWindow)
{
    ui->setupUi(this);
    basis = curBasis;
    mainWindow = mainWin;
    rateFetcher = RateFetcher(Basis::timeZone);

    ui->label_bitcoinin->setText(std::string("Amount ("   + Basis::preferredUnitString + ")").c_str());
    ui->label_rate->setText     (std::string("Rate (USD/" + Basis::preferredUnitString + ")").c_str());
    ui->dateTimeEdit->setDate(QDate::currentDate());
    switch (Basis::preferredUnit)
    {
    case Basis::BTC:  ui->lineEdit_amount->setValidator(new QDoubleValidator(0, 21000000,         8, this)); break;
    case Basis::mBTC: ui->lineEdit_amount->setValidator(new QDoubleValidator(0, 21000000000,      5, this)); break;
    case Basis::uBTC: ui->lineEdit_amount->setValidator(new QDoubleValidator(0, 21000000000000,   2, this));break;
    case Basis::sat:  ui->lineEdit_amount->setValidator(new QDoubleValidator(0, 2100000000000000, 0, this));break;
    }
    ui->lineEdit_cost->setValidator(new QDoubleValidator(0, 1000000000000, 2, this));
    ui->lineEdit_fee->setValidator(new QDoubleValidator(0, 1000000000000, 2, this));

    ui->label_error->hide();

    connect(this, SIGNAL(update_lineEdit_rate(QString)), ui->lineEdit_rate, SLOT(setText(QString)));
}

EditInputWindow::~EditInputWindow()
{
    delete ui;
    delete pendingInput;
}


void EditInputWindow::on_lineEdit_amount_textChanged(const QString &arg1)
{
    double total = ui->lineEdit_cost->text().toDouble() + ui->lineEdit_fee->text().toDouble();
    double amount = arg1.toDouble();
    emit update_lineEdit_rate(QString::number(total/amount));
}

void EditInputWindow::on_lineEdit_cost_textChanged(const QString &arg1)
{
    double total = arg1.toDouble() + ui->lineEdit_fee->text().toDouble();
    double amount = ui->lineEdit_amount->text().toDouble();
    emit update_lineEdit_rate(QString::number(total/amount));
}

void EditInputWindow::on_lineEdit_fee_textChanged(const QString &arg1)
{
    double total = ui->lineEdit_cost->text().toDouble() + arg1.toDouble();
    double amount = ui->lineEdit_amount->text().toDouble();
    emit update_lineEdit_rate(QString::number(total/amount));
}

void EditInputWindow::on_pushButton_accept_clicked()
{
    if (ui->lineEdit_amount->text().toDouble() <= 0)
    {
        ui->label_error->setText("Invalid amount");
        ui->label_error->show();
        return;
    }

    int64_t amount, cost, fee;
    switch(Basis::preferredUnit)
    {
    case Basis::BTC:  amount = ui->lineEdit_amount->text().toDouble() * 100000000;break;
    case Basis::mBTC: amount = ui->lineEdit_amount->text().toDouble() * 100000;   break;
    case Basis::uBTC: amount = ui->lineEdit_amount->text().toDouble() * 100;      break;
    case Basis::sat:  amount = ui->lineEdit_amount->text().toDouble() * 1;        break;
    }

    if (!ui->checkBox_fetchRate->isChecked())
    {
        cost = ui->lineEdit_cost->text().toDouble() * 100000000;
        fee = ui->lineEdit_fee->text().toDouble() * 100000000;
    }
    else
    {
        double rate;
        try
        {
            rate = rateFetcher.fetch(ui->comboBox_rateSource->currentText().toStdString(),
                                     ui->dateTimeEdit->text().toStdString());
        }
        catch (int error)
        {
            if      (error == -1) ui->label_error->setText("Invalid date/time");
            else if (error == -2) ui->label_error->setText("Not currently supporting averages");
            else if (error == -3) ui->label_error->setText("Error. Likely no data found");
            else                  ui->label_error->setText("Unknown error");
            ui->label_error->show();
            return;
        }

        switch (Basis::preferredUnit)
        {
            case Basis::BTC:  rate /= 1;         break;
            case Basis::mBTC: rate /= 1000;      break;
            case Basis::uBTC: rate /= 1000000;   break;
            case Basis::sat:  rate /= 100000000; break;
        }
        cost = (double)amount * rate;
        fee = 0;
    }
    pendingInput = new Basis::Input();
    pendingInput->date = DateTime(ui->dateTimeEdit->text().toStdString());
    pendingInput->amount = amount;
    pendingInput->cost = cost;
    pendingInput->fee = fee;
    basis->addInput(pendingInput);
    delete pendingInput;

    ui->label_error->hide();
    emit done();
    hide();

}

void EditInputWindow::on_checkBox_fetchRate_toggled(bool checked)
{
    if (checked)
    {
        ui->lineEdit_cost->setText("");
        ui->lineEdit_fee->setText("");
        ui->lineEdit_rate->setText("");
        ui->lineEdit_cost->setEnabled(false);
        ui->lineEdit_fee->setEnabled(false);
        ui->comboBox_rateSource->setEnabled(true);
    }
    else
    {
        ui->lineEdit_cost->setEnabled(true);
        ui->lineEdit_fee->setEnabled(true);
        ui->comboBox_rateSource->setEnabled(false);
    }
}

void EditInputWindow::on_pushButton_cancel_clicked()
{
    ui->dateTimeEdit->setDate(QDate::currentDate());
    ui->lineEdit_amount->setText("");
    ui->lineEdit_cost->setText("");
    ui->lineEdit_fee->setText("");
    ui->lineEdit_rate->setText("");
    ui->label_error->hide();
    hide();
}
