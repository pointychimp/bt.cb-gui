#-------------------------------------------------
#
# Project created by QtCreator 2014-01-11T23:19:29
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = btcb
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    jsoncpp.cpp \
    basis.cpp \
    datetime.cpp \
    intstr.cpp \
    editinputwindow.cpp \
    ratefetcher.cpp \
    editoutputwindow.cpp

HEADERS  += mainwindow.h \
    json/json-forwards.h \
    json/json.h \
    basis.h \
    datetime.h \
    intstr.h \
    editinputwindow.h \
    ratefetcher.h \
    editoutputwindow.h

FORMS    += mainwindow.ui \
    editinputwindow.ui \
    editoutputwindow.ui

LIBS += -lcurl
QMAKE_CXXFLAGS += -std=c++11
