#include "ratefetcher.h"

#include <sstream>
#include <curl/curl.h>
#include <datetime.h>
#include <intstr.h>

size_t write_data(char *ptr, size_t size, size_t nmemb, void *userdata)
{
    std::ostringstream *stream = (std::ostringstream*)userdata;
    size_t count = size * nmemb;
    stream->write(ptr, count);
    return count;
}

double RateFetcher::usdstr_double(string usd)
{
    int64_t wholePart = str_int64(usd.substr(0, usd.find(".")));
    int64_t fractPart = str_int64(usd.substr(usd.find(".")+1));
    return wholePart + (double)fractPart / 100;
}

string RateFetcher::getWebPage(string page)
{
    stringstream ss;
    CURL *curl = curl_easy_init();
    curl_easy_setopt(curl, CURLOPT_URL, page.c_str());
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &ss);
    curl_easy_perform(curl);
    return ss.str();
}

Json::Value RateFetcher::jsonEncode(string str)
{
    Json::Value json;
    static Json::Reader reader;
    reader.parse(str, json);
    return json;
}

RateFetcher::RateFetcher(int tz)
{
    timeZone = tz;
}


double RateFetcher::fetch(string exchange, string date)
{
    if (exchange.find("bitpay") != std::string::npos) exchange = "bitpay";
    DateTime dateTime(date);
    //dateTime.toUTC(timeZone);
    stringstream ss;
    if (!dateTime.isValid()) throw -1;
    ss << "http://mineoas.us/bitcoin/historic/json/" <<
            exchange << "/" <<
            dateTime.format("%Y%M%D") << "/" <<
            dateTime.format("%H%I%S") << "/" <<
            (timeZone>=0?"+":"") << timeZone;
    Json::Value json = jsonEncode(getWebPage(ss.str()));
    if (json["data"].size() != 1) throw -2; // not currently supporting average
    if (json["data"][0].isMember("error")) throw -3; // there was an error, likely no data
    return usdstr_double(json["data"][0]["price"].asString());
}
