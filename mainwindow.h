#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>

#include <editinputwindow.h>
#include <editoutputwindow.h>
#include <basis.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
signals:
    void new_basis_loaded();
private slots:
    void on_actionNew_triggered();
    void reloadBasisDisplay();
    void on_fileSelected(QString file);
    void on_pushButton_newInput_clicked();
    void on_pushButton_newOutput_clicked();
    void on_pushButton_calcGains_clicked();
    void on_actionSaveAs_triggered();
    void on_actionOpen_triggered();

    void on_actionSave_triggered();

private:
    Ui::MainWindow *ui;
    Basis::Basis currentBasis;

    QString selectedFile;

    EditInputWindow* editInputWindow;
    EditOutputWindow* editOutputWindow;
    QFileDialog* fileDialog;
};

#endif // MAINWINDOW_H
