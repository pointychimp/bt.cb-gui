#ifndef INTSTR_H
#define INTSTR_H

#include <string>

int64_t str_int64(std::string str);
int64_t str_int64date(std::string str);
std::string int64_str(int64_t num, unsigned int minNumDigits = 0);

#endif // INTSTR_H
