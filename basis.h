#ifndef BASIS_H
#define BASIS_H
#include <vector>
#include <algorithm>
#include <string>
#include <datetime.h>

namespace Basis
{

    struct Input
    {
        unsigned int id;
        DateTime date;
        std::string exchange;
        std::string txID;
        std::string notes;
        int64_t cost;
        int64_t fee;
        int64_t amount;
        Input(std::string _date, int64_t _amount, int64_t _cost) :
            date  (DateTime(_date)),
            cost  (_cost),
            fee   (0),
            amount(_amount) {}
        Input();
    };
    struct InputReference
    {
        Input* in;
        int64_t amount;
        InputReference(Input* _in, int64_t _amount) : in(_in),     amount(_amount) {}
        InputReference()                            : in(nullptr), amount(0)       {}
    };
    struct Output
    {
        unsigned int id;
        DateTime date;
        std::string exchange;
        std::string txID;
        std::string notes;
        int64_t proceeds;
        int64_t amount;
        std::vector<InputReference> inputs;

        unsigned int numRefs() {return inputs.size();}

        Output(std::string _date, int64_t _amount, int64_t _proceeds) :
            date     (DateTime(_date)),
            proceeds (_proceeds),
            amount   (_amount) {}
        Output();
    };

    enum enumBTCunit {BTC, mBTC, uBTC, sat};
    extern enumBTCunit preferredUnit;
    extern std::string preferredUnitString;

    extern int timeZone;

    // takes an int64_t storing a USD value
        // essentially converts
        // 122333333 (int64_t) to 1 and 22333333
        // then "1.22" (std::string, rounded to cent)
        // for displaying only
    std::string usdint64_str(int64_t usd);
    // takes an int64_t storing a USD value
        // only returns double
        // instead of std::string
        // use this when further calculations are necessary
    double usdint64_double(int64_t usd);
    // takes an int64_t storing a satoshi value
        // essentially converts
        // 123456789 (int64_t) (# of satoshis)
        // to 1.23456789 (std::string) (# of BTC, or preferred unit)
        // for displaying only
    std::string satint64_prefstr(int64_t sat);
    // similar to satint64_prefstr,
        // only returns double (in preferred unit)
        // instead of std::string
        // use this when further calculations are necessary
    double satint64_prefdouble(int64_t sat);

    bool operator< (const Input& a, const Input& b);
    bool operator> (const Input& a, const Input& b);
    bool operator<=(const Input& a, const Input& b);
    bool operator>=(const Input& a, const Input& b);
    bool operator==(const Input& a, const Input& b);
    bool operator!=(const Input& a, const Input& b);
    bool operator< (const Output& a, const Output& b);
    bool operator> (const Output& a, const Output& b);
    bool operator<=(const Output& a, const Output& b);
    bool operator>=(const Output& a, const Output& b);
    bool operator==(const Output& a, const Output& b);
    bool operator!=(const Output& a, const Output& b);

    class Basis
    {
    private:
        std::vector<Input> inputs;
        std::vector<Output> outputs;
        bool needRefresh;
        int64_t sum(std::vector<Input> in);
        int64_t sum(std::vector<Output> out);

    public:
        void         addInput(Input in);
        void         addInput(Input *in);
        void         addOutput(Output out);
        void         addOutput(Output *out);
        void         resetAll();
        unsigned int getNumInputs()            {return inputs.size();}
        unsigned int getNumOutputs()           {return outputs.size();}
        const Input  getInput(unsigned int i)  {return inputs.at(i);}
        const Output getOutput(unsigned int i) {return outputs.at(i);}
        void         refresh(bool force = false);
        Basis();
        ~Basis();
    };

    std::ostream&  operator<<(std::ostream&  os, Basis  basis);
    std::ostream&  operator<<(std::ostream&  os, Input  input);
    std::ostream&  operator<<(std::ostream&  os, Output output);
    std::ifstream& operator>>(std::ifstream& is, Basis*  basis);

}

#endif // BASIS_H
