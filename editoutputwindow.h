#ifndef EDITOUTPUTWINDOW_H
#define EDITOUTPUTWINDOW_H

#include <QWidget>
#include <basis.h>
#include <ratefetcher.h>

namespace Ui {
class EditOutputWindow;
}

class EditOutputWindow : public QWidget
{
    Q_OBJECT
    
public:
    explicit EditOutputWindow(
            Basis::Basis *curBasis,
            QWidget *mainWin,
            QWidget *parent = 0);
    ~EditOutputWindow();
signals:
    void done();
private slots:
    void on_pushButton_accept_clicked();
    void on_pushButton_cancel_clicked();
    void on_radioButton_rate_toggled(bool checked);
    void on_radioButton_proceeds_toggled(bool checked);

    void on_lineEdit_rate_textEdited(const QString &arg1);

    void on_lineEdit_proceeds_textEdited(const QString &arg1);

    void on_lineEdit_amount_textEdited(const QString &arg1);

    void on_checkBox_fetchRate_toggled(bool checked);

private:
    Ui::EditOutputWindow *ui;

    Basis::Output* pendingOutput;
    Basis::Basis* basis;
    QWidget* mainWindow;
    RateFetcher rateFetcher;
};

#endif // EDITOUTPUTWINDOW_H
