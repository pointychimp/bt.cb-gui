#include "editoutputwindow.h"
#include "ui_editoutputwindow.h"

EditOutputWindow::EditOutputWindow(
        Basis::Basis *curBasis,
        QWidget *mainWin,
        QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EditOutputWindow)
{
    ui->setupUi(this);

    basis = curBasis;
    mainWindow = mainWin;
    rateFetcher = RateFetcher(Basis::timeZone);

    ui->label_bitcoinout->setText(std::string("Amount ("   + Basis::preferredUnitString + ")").c_str());
    ui->label_rate->setText      (std::string("Rate (USD/" + Basis::preferredUnitString + ")").c_str());
    ui->dateTimeEdit->setDate(QDate::currentDate());
    switch (Basis::preferredUnit)
    {
    case Basis::BTC:  ui->lineEdit_amount->setValidator(new QDoubleValidator(0, 21000000,         8, this)); break;
    case Basis::mBTC: ui->lineEdit_amount->setValidator(new QDoubleValidator(0, 21000000000,      5, this)); break;
    case Basis::uBTC: ui->lineEdit_amount->setValidator(new QDoubleValidator(0, 21000000000000,   2, this)); break;
    case Basis::sat:  ui->lineEdit_amount->setValidator(new QDoubleValidator(0, 2100000000000000, 0, this)); break;
    }
    ui->lineEdit_proceeds->setValidator(new QDoubleValidator(0, 1000000000000, 2, this));
    ui->lineEdit_rate->setValidator    (new QDoubleValidator(0, 1000000000000, 2, this));

    ui->label_error->hide();
}

EditOutputWindow::~EditOutputWindow()
{
    delete ui;
    delete pendingOutput;
}

void EditOutputWindow::on_pushButton_accept_clicked()
{
    if (ui->lineEdit_amount->text().toDouble() <= 0)
    {
        ui->label_error->setText("Invalid amount");
        ui->label_error->show();
        return;
    }

    int64_t amount, proceeds;
    switch(Basis::preferredUnit)
    {
    case Basis::BTC:  amount = ui->lineEdit_amount->text().toDouble() * 100000000;break;
    case Basis::mBTC: amount = ui->lineEdit_amount->text().toDouble() * 100000;   break;
    case Basis::uBTC: amount = ui->lineEdit_amount->text().toDouble() * 100;      break;
    case Basis::sat:  amount = ui->lineEdit_amount->text().toDouble() * 1;        break;
    }
    if (ui->radioButton_rate->isChecked())
    {
        if (!ui->checkBox_fetchRate->isChecked())
        {
            // rate was entered, calc proceeds
            proceeds = ui->lineEdit_rate->text().toDouble() * amount;
        }
        else
        {
            // need to fetch
            double rate;
            try
            {
                rate = rateFetcher.fetch(ui->comboBox_rateSource->currentText().toStdString(),
                                         ui->dateTimeEdit->text().toStdString());
            }
            catch (int error)
            {
                if      (error == -1) ui->label_error->setText("Invalid date/time");
                else if (error == -2) ui->label_error->setText("Not currently supporting averages");
                else if (error == -3) ui->label_error->setText("Error. Likely no data found");
                else                  ui->label_error->setText("Unknown error");
                ui->label_error->show();
                return;
            }

            switch (Basis::preferredUnit)
            {
                case Basis::BTC:  rate /= 1;         break;
                case Basis::mBTC: rate /= 1000;      break;
                case Basis::uBTC: rate /= 1000000;   break;
                case Basis::sat:  rate /= 100000000; break;
            }
            proceeds = rate * amount;
        }
    }
    else if (ui->radioButton_proceeds->isChecked())
    {
        proceeds = ui->lineEdit_proceeds->text().toDouble() * 100000000;
    }
    else
    {
        ui->label_error->setText("Need more information");
        ui->label_error->show();
        return;
    }

    pendingOutput = new Basis::Output();
    pendingOutput->date = DateTime(std::string(ui->dateTimeEdit->text().toStdString()));
    pendingOutput->amount = amount;
    pendingOutput->proceeds = proceeds;
    basis->addOutput(pendingOutput);

    delete pendingOutput;

    ui->label_error->hide();

    emit done();
    hide();
}

void EditOutputWindow::on_pushButton_cancel_clicked()
{
    ui->dateTimeEdit->setDate(QDate::currentDate());
    ui->lineEdit_amount->setText("");
    ui->lineEdit_proceeds->setText("");
    ui->lineEdit_rate->setText("");
    ui->label_error->hide();
    hide();
}

void EditOutputWindow::on_radioButton_rate_toggled(bool checked)
{
    if (checked)
    {
        ui->lineEdit_proceeds->setEnabled(false);
        ui->checkBox_fetchRate->setEnabled(true);
        ui->lineEdit_rate->setEnabled((ui->checkBox_fetchRate->isChecked()?false:true));
        ui->comboBox_rateSource->setEnabled((ui->checkBox_fetchRate->isChecked()?true:false));
    }
    else
    {
        ui->lineEdit_rate->setEnabled(false);
        ui->checkBox_fetchRate->setEnabled(false);
        ui->lineEdit_proceeds->setEnabled(true);
    }
}

void EditOutputWindow::on_radioButton_proceeds_toggled(bool checked)
{
    on_radioButton_rate_toggled(!checked);
}


void EditOutputWindow::on_lineEdit_rate_textEdited(const QString &arg1)
{
    ui->lineEdit_proceeds->setText(QString::number(arg1.toDouble() * ui->lineEdit_amount->text().toDouble()));
}

void EditOutputWindow::on_lineEdit_proceeds_textEdited(const QString &arg1)
{
    ui->lineEdit_rate->setText(QString::number(arg1.toDouble() / ui->lineEdit_amount->text().toDouble()));
}

void EditOutputWindow::on_lineEdit_amount_textEdited(const QString &arg1)
{
    if (ui->lineEdit_proceeds->isEnabled())
    {
        ui->lineEdit_rate->setText(QString::number(ui->lineEdit_proceeds->text().toDouble() / arg1.toDouble()));
    }
    else if (ui->lineEdit_rate->isEnabled())
    {
        ui->lineEdit_proceeds->setText(QString::number(ui->lineEdit_rate->text().toDouble() * arg1.toDouble()));
    }

}

void EditOutputWindow::on_checkBox_fetchRate_toggled(bool checked)
{
    ui->lineEdit_rate->setText("");
    ui->lineEdit_rate->setEnabled(!checked);
    ui->comboBox_rateSource->setEnabled(checked);
}
