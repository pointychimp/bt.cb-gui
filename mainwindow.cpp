#include "mainwindow.h"
#include "ui_mainwindow.h"

#include<QMessageBox>
#include <string>
#include <sstream>
#include <intstr.h>
#include <iostream>
#include <fstream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    editInputWindow = new EditInputWindow(&currentBasis, this);
    editOutputWindow = new EditOutputWindow(&currentBasis, this);
    fileDialog = new QFileDialog(this);

    connect(editInputWindow, SIGNAL(done()), this, SLOT(reloadBasisDisplay()));
    connect(editOutputWindow, SIGNAL(done()), this, SLOT(reloadBasisDisplay()));
    connect(this, SIGNAL(new_basis_loaded()), this, SLOT(reloadBasisDisplay()));
    connect(fileDialog, SIGNAL(fileSelected(QString)), this, SLOT(on_fileSelected(QString)));

    currentBasis.addInput(Basis::Input("20140101", 100000000, 100000000000));
    currentBasis.addInput(Basis::Input("20140101", 100000000, 100000000000));
    currentBasis.addInput(Basis::Input("20140101", 100000000, 100000000000));
    currentBasis.addInput(Basis::Input("20140101", 100000000, 100000000000));
    currentBasis.addInput(Basis::Input("20140101", 100000000, 100000000000));
    currentBasis.addInput(Basis::Input("20140101", 100000000, 100000000000));
    currentBasis.addInput(Basis::Input("20140101", 100000000, 100000000000));
    currentBasis.addInput(Basis::Input("20140101", 100000000, 100000000000));
    currentBasis.addInput(Basis::Input("20140101", 100000000, 100000000000));
    currentBasis.addInput(Basis::Input("20140101", 100000000, 100000000000));
    currentBasis.addInput(Basis::Input("20140101", 100000000, 100000000000));
    currentBasis.addInput(Basis::Input("20140101", 100000000, 100000000000));
    currentBasis.addInput(Basis::Input("20140101", 100000000, 100000000000));

    currentBasis.addOutput(Basis::Output("20140102", 100000000, 200000000000));
    currentBasis.addOutput(Basis::Output("20140102", 800000000, 200000000000));
    currentBasis.addOutput(Basis::Output("20140102", 100000000, 200000000000));
    currentBasis.addOutput(Basis::Output("20140102", 100000000, 200000000000));
    currentBasis.addOutput(Basis::Output("20140102", 100000000, 200000000000));
    currentBasis.addOutput(Basis::Output("20140102", 100000000, 200000000000));

    currentBasis.refresh(true);

    emit new_basis_loaded();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete editInputWindow;
}

void MainWindow::on_actionNew_triggered()
{
    currentBasis = Basis::Basis();

    emit new_basis_loaded();
}

void MainWindow::reloadBasisDisplay()
{
    ui->actionSave->setEnabled(true);
    try
    {
        currentBasis.refresh();
    }
    catch (int err)
    {
        QMessageBox messBox;
        messBox.setFixedSize(500,200);
        switch (err)
        {
        case 1:
            messBox.critical(0,"Warning","Outputs greater than Inputs");
            break;
        default:
            messBox.critical(0, "Error","Unknown error occured");
        }
        messBox.exec();
    }
    // inputs
        ui->tableInputs->setRowCount(currentBasis.getNumInputs());
        ui->tableInputs->setColumnCount(10);
        QStringList inputsStrList; // set column titles
        inputsStrList<<"id"<<"Date"<<"Coins"<<"Cost"<<"Fees"<<"Tot Cost"<<"Rate (inc Fees)"<<"Agg Coins In"<<"Snapshot Value"<<"Agg Invested";
        ui->tableInputs->setHorizontalHeaderLabels(inputsStrList);
        ui->tableInputs->verticalHeader()->setVisible(false); // hides row counter on left
        ui->tableInputs->setEditTriggers(QAbstractItemView::NoEditTriggers); // no editing
    // outputs
        unsigned int numRows = 0;
        for (unsigned int i = 0; i < currentBasis.getNumOutputs(); i++)
        {
            Basis::Output out = currentBasis.getOutput(i);
            numRows += out.numRefs();
        }
        ui->tableOutputs->setRowCount(numRows);
        ui->tableOutputs->setColumnCount(8);
        QStringList outputsStrList;
        outputsStrList<<"id"<<"Date"<<"Coins"<<"Rate"<<"Buys"<<"Coins"<<"Proceeds"<<"Gain/Loss";
        ui->tableOutputs->setHorizontalHeaderLabels(outputsStrList);
        ui->tableOutputs->verticalHeader()->setVisible(false); // hide row counter on left
        ui->tableOutputs->setEditTriggers(QAbstractItemView::NoEditTriggers); // no editing

    int64_t aggCoinsIn = 0;
    int64_t aggInvested = 0;
// inputs
    for (unsigned int i = 0; i < currentBasis.getNumInputs(); i++)
    {
        Basis::Input in = currentBasis.getInput(i);
    // ids
        std::stringstream ss;
        std::string str;
        ss << "I:" << 1000+in.id;
        ss >> str;
        ss.str(""); ss.clear();
        ui->tableInputs->setItem(i,0, new QTableWidgetItem(str.c_str()));
        ui->tableInputs->item(i,0)->setTextAlignment(Qt::AlignRight | Qt::AlignCenter);
    // dates
        ui->tableInputs->setItem(i,1, new QTableWidgetItem(in.date.format("%Y-%M-%D %H:%I:%S").c_str()));
        ui->tableInputs->item(i,1)->setTextAlignment(Qt::AlignRight | Qt::AlignCenter);
    // coins
        ui->tableInputs->setItem(i,2, new QTableWidgetItem(Basis::satint64_prefstr(in.amount).c_str()));
        ui->tableInputs->item(i,2)->setTextAlignment(Qt::AlignRight | Qt::AlignCenter);
    // cost
        str = "$" + Basis::usdint64_str(in.cost);
        ui->tableInputs->setItem(i,3, new QTableWidgetItem(str.c_str()));
        ui->tableInputs->item(i,3)->setTextAlignment(Qt::AlignRight | Qt::AlignCenter);
    // fees
        str = "$" + Basis::usdint64_str(in.fee);
        ui->tableInputs->setItem(i,4, new QTableWidgetItem(str.c_str()));
        ui->tableInputs->item(i,4)->setTextAlignment(Qt::AlignRight | Qt::AlignCenter);
    // tot cost
        str = "$" + Basis::usdint64_str(in.cost + in.fee);
        ui->tableInputs->setItem(i,5, new QTableWidgetItem(str.c_str()));
        ui->tableInputs->item(i,5)->setTextAlignment(Qt::AlignRight | Qt::AlignCenter);
    // rate (inc fees)
        double rate = Basis::usdint64_double(in.cost + in.fee)/Basis::satint64_prefdouble(in.amount);
        ss << "$" << rate;
        ss >> str;
        ss.str(""); ss.clear();
        if (str.find(".") == std::string::npos) str += ".";
        int spaceBetweenDotAndEnd = str.length()-1-str.find(".");
        if (spaceBetweenDotAndEnd < 2) str += std::string(2-spaceBetweenDotAndEnd, '0');
        ui->tableInputs->setItem(i,6, new QTableWidgetItem(str.c_str()));
        ui->tableInputs->item(i,6)->setTextAlignment(Qt::AlignRight | Qt::AlignCenter);
    // agg coin in
        aggCoinsIn += in.amount;
        ui->tableInputs->setItem(i,7, new QTableWidgetItem(Basis::satint64_prefstr(aggCoinsIn).c_str()));
        ui->tableInputs->item(i,7)->setTextAlignment(Qt::AlignRight | Qt::AlignCenter);
    // snapshot value
        ss << "$" << Basis::satint64_prefdouble(aggCoinsIn)*rate;
        ss >> str;
        ss.str(""); ss.clear();
        if (str.find(".") == std::string::npos) str += ".";
        spaceBetweenDotAndEnd = str.length()-1-str.find(".");
        if (spaceBetweenDotAndEnd < 2) str += std::string(2-spaceBetweenDotAndEnd, '0');
        ui->tableInputs->setItem(i,8, new QTableWidgetItem(str.c_str()));
        ui->tableInputs->item(i,8)->setTextAlignment(Qt::AlignRight | Qt::AlignCenter);
    // invested value
        aggInvested += in.cost + in.fee;
        str = "$" + Basis::usdint64_str(aggInvested);
        ui->tableInputs->setItem(i,9, new QTableWidgetItem(str.c_str()));
        ui->tableInputs->item(i,9)->setTextAlignment(Qt::AlignRight | Qt::AlignCenter);
    }
// outputs
    unsigned int row = 0;
    for (unsigned int i = 0; i < currentBasis.getNumOutputs(); i++, row++)
    {
        Basis::Output out = currentBasis.getOutput(i);
    // ids
        std::stringstream ss;
        std::string str;
        ss << "O:" << 1000+out.id;
        ss >> str;
        ss.str(""); ss.clear();
        ui->tableOutputs->setItem(row,0, new QTableWidgetItem(str.c_str()));
        ui->tableOutputs->item(row,0)->setTextAlignment(Qt::AlignRight | Qt::AlignCenter);
    // dates
        ui->tableOutputs->setItem(row,1, new QTableWidgetItem(out.date.format("%Y-%M-%D %H:%I%S").c_str()));
        ui->tableOutputs->item(row,1)->setTextAlignment(Qt::AlignRight | Qt::AlignCenter);
    // coins
        ui->tableOutputs->setItem(row,2, new QTableWidgetItem(Basis::satint64_prefstr(out.amount).c_str()));
        ui->tableOutputs->item(row,2)->setTextAlignment(Qt::AlignRight | Qt::AlignCenter);
    // rate
        double rate = Basis::usdint64_double(out.proceeds)/Basis::satint64_prefdouble(out.amount);
        ss << "$" << rate;
        ss >> str;
        ss.str(""); ss.clear();
        if (str.find(".") == std::string::npos) str += ".";
        int spaceBetweenDotAndEnd = str.length()-1-str.find(".");
        if (spaceBetweenDotAndEnd < 2) str += std::string(2-spaceBetweenDotAndEnd, '0');
        ui->tableOutputs->setItem(row,3, new QTableWidgetItem(str.c_str()));
    // buy info
        for (unsigned int j = 0; j < out.inputs.size(); j++)
        {
            Basis::InputReference inRef = out.inputs.at(j);
            ss << "I:" << 1000 + inRef.in->id;
            ss >> str;
            ss.str(""); ss.clear();
        // ref buy id
            ui->tableOutputs->setItem(row,4, new QTableWidgetItem(str.c_str()));
            ui->tableOutputs->item(row,4)->setTextAlignment(Qt::AlignRight | Qt::AlignCenter);
        // ref buy amount
            ui->tableOutputs->setItem(row,5, new QTableWidgetItem(Basis::satint64_prefstr(inRef.amount).c_str()));
            ui->tableOutputs->item(row,5)->setTextAlignment(Qt::AlignRight | Qt::AlignCenter);
        // ref buy proceeds
            ss << "$" << Basis::usdint64_str(rate * inRef.amount).c_str();
            ss >> str;
            ss.str(""); ss.clear();
            ui->tableOutputs->setItem(row,6, new QTableWidgetItem(str.c_str()));
            ui->tableOutputs->item(row,6)->setTextAlignment(Qt::AlignRight | Qt::AlignCenter);
        // gain (loss)
            double inRate = Basis::usdint64_double(inRef.in->cost + inRef.in->fee)/Basis::satint64_prefdouble(inRef.in->amount);
            std::string gain = Basis::usdint64_str((rate-inRate) * inRef.amount);
            if (gain.at(0) == '-') gain = "- $" + gain.substr(1);
            else gain = "  $" + gain;
            ui->tableOutputs->setItem(row,7, new QTableWidgetItem(gain.c_str()));
            ui->tableOutputs->item(row,7)->setTextAlignment(Qt::AlignRight | Qt::AlignCenter);
            if (gain.at(0) == '-')
                ui->tableOutputs->item(row,7)->setForeground(QColor::fromRgb(255,0,0));
            if (j + 1 < out.inputs.size()) row++;
        }
    }

// inputs
    ui->tableInputs->resizeColumnsToContents();
    ui->tableInputs->resizeRowsToContents();
// outputs
    ui->tableOutputs->resizeColumnsToContents();
    ui->tableOutputs->resizeRowsToContents();
}

void MainWindow::on_fileSelected(QString file)
{
    if (file.toStdString().find(".btcb") == std::string::npos) file += ".btcb";
    selectedFile = file;
}

void MainWindow::on_pushButton_newInput_clicked()
{
    editInputWindow->show();
}

void MainWindow::on_pushButton_newOutput_clicked()
{
    editOutputWindow->show();
}

void MainWindow::on_pushButton_calcGains_clicked()
{
    // force refresh
    currentBasis.refresh(true);
}

void MainWindow::on_actionSaveAs_triggered()
{
    QStringList filters;
    filters << "bt.cb data files (*.btcb)" << "All files (*)";

    fileDialog->setAcceptMode(QFileDialog::AcceptSave);
    fileDialog->setFileMode(QFileDialog::AnyFile);
    fileDialog->setDefaultSuffix("btcb"); // doesn't work
    fileDialog->setNameFilters(filters);
    fileDialog->exec();

    std::ofstream outputFile;
    outputFile.open(selectedFile.toStdString());
    if (!outputFile.is_open())
    {
        QMessageBox messBox;
        messBox.setFixedSize(500,200);
        messBox.critical(0,"Error","Could not open file for saving.");
        return;
    }
    outputFile << currentBasis;
    outputFile.close();
    QMessageBox messBox;
    messBox.setFixedSize(500,200);
    messBox.information(0, "Success", QString::fromStdString("File saved as " + selectedFile.toStdString().substr(selectedFile.toStdString().find_last_of("/")+1)));
    ui->actionSave->setEnabled(false);
}

void MainWindow::on_actionOpen_triggered()
{
    QStringList filters;
    filters << "bt.cb data files (*.btcb)" << "All files (*)";

    fileDialog->setAcceptMode(QFileDialog::AcceptOpen);
    fileDialog->setFileMode(QFileDialog::ExistingFile);
    fileDialog->setNameFilters(filters);
    fileDialog->exec();

    std::ifstream inputFile;
    inputFile.open(selectedFile.toStdString());
    if (!inputFile.is_open())
    {
        QMessageBox messBox;
        messBox.setFixedSize(500,200);
        messBox.critical(0,"Error","Could not open file.");
        return;
    }
    try {inputFile >> &currentBasis;}
    catch (int err)
    {
        std::string str;
        switch (err)
        {
        case 1: str = "Parsing error. Inputs not found."; break;
        case 2: str = "Parsing error. An incorrect date format"; break;
        case 3: str = "Parsing error. Outputs not found."; break;
        default: str = "Unknown error occured.";
        }
        QMessageBox messBox;
        messBox.setFixedSize(500,200);
        messBox.critical(0, "Error", QString::fromStdString(str));
        return;
    }
    currentBasis.refresh(true);
    emit reloadBasisDisplay();
    ui->actionSave->setEnabled(false);
    /*QMessageBox messBox;
    messBox.setFixedSize(500,200);
    messBox.information(0, "Success", "File opened");*/
}

void MainWindow::on_actionSave_triggered()
{
    std::ofstream outputFile;
    outputFile.open(selectedFile.toStdString());
    if (!outputFile.is_open())
    {
        QMessageBox messBox;
        messBox.setFixedSize(500,200);
        messBox.critical(0,"Error","Could not open file for saving.");
        return;
    }
    outputFile << currentBasis;
    outputFile.close();
    QMessageBox messBox;
    messBox.setFixedSize(500,200);
    messBox.information(0, "Success", QString::fromStdString("File saved as " + selectedFile.toStdString().substr(selectedFile.toStdString().find_last_of("/")+1)));
    ui->actionSave->setEnabled(false);
}
