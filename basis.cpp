#include "basis.h"

#include <sstream>
#include <intstr.h>
#include <iostream>
#include <fstream>

namespace Basis
{

    enumBTCunit preferredUnit;
    std::string preferredUnitString;
    int timeZone;

    Input::Input() {}
    Output::Output() {}

    bool operator< (const Input& a,  const Input& b)  {return a.date <  b.date;}
    bool operator==(const Input& a,  const Input& b)  {return a.date == b.date;}
    bool operator> (const Input& a,  const Input& b)  {return   b <  a ;}
    bool operator<=(const Input& a,  const Input& b)  {return !(a >  b);}
    bool operator>=(const Input& a,  const Input& b)  {return !(a <  b);}
    bool operator!=(const Input& a,  const Input& b)  {return !(a == b);}
    bool operator< (const Output& a, const Output& b) {return a.date <  b.date;}
    bool operator==(const Output& a, const Output& b) {return a.date == b.date;}
    bool operator> (const Output& a, const Output& b) {return   b <  a ;}
    bool operator<=(const Output& a, const Output& b) {return !(a >  b);}
    bool operator>=(const Output& a, const Output& b) {return !(a <  b);}
    bool operator!=(const Output& a, const Output& b) {return !(a == b);}

    int64_t Basis::sum(std::vector<Input> in)
    {
        int64_t total = 0;
        for (unsigned int i = 0; i < in.size(); i++)
        {
            total += in.at(i).amount;
        }
        return total;
    }
    int64_t Basis::sum(std::vector<Output> out)
    {
        int64_t total = 0;
        for (unsigned int i = 0; i < out.size(); i++)
        {
            total += out.at(i).amount;
        }
        return total;
    }

    std::string usdint64_str(int64_t usd)
    {
        int64_t wholePart = usd / 100000000;
        int64_t fractPart = usd % 100000000;
        //fractPart = ((fractPart+500000) / 1000000) * 1000000; // rounds fractPart to nearest cent with 6 trailing zeros
        fractPart = (fractPart+500000) / 1000000; // not multiplying it after removes the trailing zeros.
        return int64_str(wholePart) + "." + int64_str(fractPart, 2);
    }
    double usdint64_double(int64_t usd)
    {
        int64_t wholePart = usd / 100000000;
        int64_t fractPart = usd % 100000000;
        fractPart = ((fractPart+500000) / 1000000) * 1000000;
        return wholePart + (double)fractPart / (double)100000000;
    }
    std::string satint64_prefstr(int64_t sat)
    {
        int64_t factor;
        if      (preferredUnit ==  BTC) factor = 100000000;
        else if (preferredUnit == mBTC) factor = 100000;
        else if (preferredUnit == uBTC) factor = 100;
        else                            factor = 1;
        int64_t wholePart =                    sat / factor;
        int64_t fractPart = (factor == 1 ? 0 : sat % factor);
        return int64_str(wholePart) + (fractPart ? "."+int64_str(fractPart) : "");
    }
    double satint64_prefdouble(int64_t sat)
    {
        int64_t factor;
        if      (preferredUnit ==  BTC) factor = 100000000;
        else if (preferredUnit == mBTC) factor = 100000;
        else if (preferredUnit == uBTC) factor = 100;
        else                            factor = 1;
        int64_t wholePart =                    sat / factor;
        int64_t fractPart = (factor == 1 ? 0 : sat % factor);
        return wholePart + (double)fractPart / (double)factor;
    }

    void Basis::addInput(Input in)
    {
        inputs.push_back(in);
        needRefresh = true;
    }
    void Basis::addInput(Input *in)
    {
        Input newInput = *in;
        addInput(newInput);
    }
    void Basis::addOutput(Output out)
    {
        outputs.push_back(out);
        needRefresh = true;
    }
    void Basis::addOutput(Output *out)
    {
        Output newOutput = *out;
        addOutput(newOutput);
    }

    void Basis::resetAll()
    {
        inputs.resize(0);
        outputs.resize(0);
        preferredUnit = BTC;
        preferredUnitString = "BTC";
        timeZone = 0;
        needRefresh = false;
    }

    void Basis::refresh(bool force)
    {
        if (needRefresh || force)
        {
            std::sort(inputs.begin(), inputs.end());
            std::sort(outputs.begin(), outputs.end());

            for (unsigned int i = 0; i < inputs.size(); i++)
                {inputs.at(i).id = i;}
            for (unsigned int i = 0; i < outputs.size(); i++)
                {outputs.at(i).id = i;}

            if (sum(inputs) < sum(outputs))
                throw 1;

            if (inputs.size() > 0)
            {
                if (outputs.size() > 0)
                {
                    // remove all input references in outputs
                    for (unsigned int i = 0; i < outputs.size(); i++)
                        {outputs.at(i).inputs.clear();}

                    unsigned int currentInput = 0;
                    Input* activeInput = &inputs.at(currentInput);
                    int64_t actInRemaining = activeInput->amount;
                    int64_t stillNeed;
                    for (unsigned int i = 0; i < outputs.size(); i++)
                    {
                        stillNeed = outputs.at(i).amount;
                        while (stillNeed > actInRemaining)
                        {
                            stillNeed -= actInRemaining;
                            outputs.at(i).inputs.push_back(InputReference(activeInput, actInRemaining));
                            activeInput = &inputs.at(++currentInput);
                            actInRemaining = activeInput->amount;
                            //std::cout << "o" << i << " eats i" << currentInput << std::endl;
                        }
                        outputs.at(i).inputs.push_back(InputReference(activeInput, stillNeed));
                        actInRemaining -= stillNeed;
                        if (actInRemaining <= 0 && currentInput + 1 < inputs.size())
                        {
                            activeInput = &inputs.at(++currentInput);
                            actInRemaining = activeInput->amount;
                        }
                        //std::cout << "o" << i << " finishes with i" << currentInput << " (remaining " << actInRemaining << ")" << std::endl;
                    }
                    //std::cout << "----------" << std::endl;
                }
                else
                {
                    // no outputs, nothing to calculate
                }

            }
            else
            {
                // no inputs, nothing to calculate
            }


        }
        needRefresh = false;
    }

    Basis::Basis()
    {
        timeZone = 0;
        preferredUnit = BTC;
        if      (preferredUnit ==  BTC) preferredUnitString =  "BTC";
        else if (preferredUnit == mBTC) preferredUnitString = "mBTC";
        else if (preferredUnit == uBTC) preferredUnitString = "uBTC";
        else if (preferredUnit ==  sat) preferredUnitString =  "sat";

        needRefresh = true;
    }
    Basis::~Basis()
    {

    }

    std::ostream &operator<<(std::ostream &os, Basis basis)
    {
        os << "---inputs---" << std::endl;
        for (unsigned int i = 0; i < basis.getNumInputs(); i++)
        {
            os << basis.getInput(i);
        }
        os << "---/inputs---" << std::endl;
        os << "---outputs---" << std::endl;
        for (unsigned int i = 0; i < basis.getNumOutputs(); i++)
        {
            os << basis.getOutput(i);
        }
        os << "---/outputs---" << std::endl;
        return os;
    }
    std::ostream &operator<<(std::ostream &os, Input input)
    {
        //os << input.id << std::endl;
        os << input.date.format("%Y%M%D%H%I%S") << std::endl;
        os << input.amount << std::endl;
        os << input.cost << std::endl;
        os << input.fee << std::endl;
        os << (input.txID     != "" ? "\""+input.txID+"\""     : "\"null\"") << std::endl;
        os << (input.notes    != "" ? "\""+input.notes+"\""    : "\"null\"") << std::endl;
        os << (input.exchange != "" ? "\""+input.exchange+"\"" : "\"null\"") << std::endl;
        return os;
    }
    std::ostream &operator<<(std::ostream &os, Output output)
    {
        //os << output.id << std::endl;
        os << output.date.format("%Y%M%D%H%I%S") << std::endl;
        os << output.amount << std::endl;
        os << output.proceeds << std::endl;
        os << (output.txID     != "" ? "\""+output.txID+"\""     : "null") << std::endl;
        os << (output.notes    != "" ? "\""+output.notes+"\""    : "null") << std::endl;
        os << (output.exchange != "" ? "\""+output.exchange+"\"" : "null") << std::endl;
        return os;
    }
    std::ifstream &operator>>(std::ifstream &is, Basis* basis)
    {
        basis->resetAll();
        enum ErrorEnum {NOINPUTS, BADDATEFORMAT, NOOUTPUTS};
        std::string bufStr;
        is >> bufStr;
        if (bufStr != "---inputs---") throw NOINPUTS;
        is >> bufStr;
        do
        {
            Input pendingInput;
            pendingInput.date = DateTime(bufStr);
            if (!pendingInput.date.isValid()) throw BADDATEFORMAT;
            is >> pendingInput.amount;
            is >> pendingInput.cost;
            is >> pendingInput.fee;
            is >> pendingInput.txID;
            is >> pendingInput.notes;
            is >> pendingInput.exchange;
            pendingInput.txID     = pendingInput.txID.    substr(0, pendingInput.txID.    length()-2);
            pendingInput.notes    = pendingInput.notes.   substr(0, pendingInput.notes.   length()-2);
            pendingInput.exchange = pendingInput.exchange.substr(0, pendingInput.exchange.length()-2);
            if (pendingInput.txID     == "null") pendingInput.txID     = "";
            if (pendingInput.notes    == "null") pendingInput.notes    = "";
            if (pendingInput.exchange == "null") pendingInput.exchange = "";
            basis->addInput(pendingInput);
            is >> bufStr;
        } while (bufStr != "---/inputs---");
        is >> bufStr;
        if (bufStr != "---outputs---") throw NOOUTPUTS;
        is >> bufStr;
        do
        {
            Output pendingOutput;
            pendingOutput.date = DateTime(bufStr);
            if (!pendingOutput.date.isValid()) throw BADDATEFORMAT;
            is >> pendingOutput.amount;
            is >> pendingOutput.proceeds;
            is >> pendingOutput.txID;
            is >> pendingOutput.notes;
            is >> pendingOutput.exchange;
            if (pendingOutput.txID     == "\"null\"") pendingOutput.txID     = "";
            if (pendingOutput.notes    == "\"null\"") pendingOutput.notes    = "";
            if (pendingOutput.exchange == "\"null\"") pendingOutput.exchange = "";
            basis->addOutput(pendingOutput);
            is >> bufStr;
        } while (bufStr != "---/outputs---");
        return is;
    }

}
