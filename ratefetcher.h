#ifndef RATEFETCHER_H
#define RATEFETCHER_H
#include <string>
#include <json/json.h>
#include <json/json-forwards.h>

using namespace std;

class RateFetcher
{
private:
    string getWebPage(string page);
    Json::Value jsonEncode(string str);
    double usdstr_double(string usd);
    int timeZone;
public:
    double fetch(string exchange, string date);
    RateFetcher(int tz = 0);
};

#endif // RATEFETCHER_H
